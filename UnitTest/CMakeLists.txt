target_sources(${PROJECT_NAME} PRIVATE
        CatchTest.cpp
        TestColoring.cpp
        TestMatrix.cpp
        TestMode.cpp
        TestNURBS.cpp
        TestQuaternion.cpp
        TestShape.cpp
        TestString.cpp
        TestTensor.cpp
        TestUtility.cpp
        TestIntegration.cpp
        )